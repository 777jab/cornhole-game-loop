// game_loop.c
#include "stdlib.h"
#include "stdint.h"
#include "stdbool.h"
#include "math.h"
#include "stdio.h"

/* number of bags in the cornhole game */
#define NUM_BAGS 8

/* bags which move less than this distance probably didn't actually move */
#define NOISE_DISTANCE 10 

/* bags closer than this distance have been thrown */
#define THROWN_DISTANCE 5000

/* the weight of one bag */
#define BAG_WEIGHT 300

typedef enum {
    UNTHROWN,
    ON_BOARD,
    OFF_BOARD,
    IN_HOLE,
} bag_state_t;

bool all_is_still(const int32_t *new_distances, const int32_t *old_distances){
    bool still = true;
    for(uint8_t i = 0; i < NUM_BAGS;i++){
        if ( /* if bag is thrown and bag has moved */
            new_distances[i] < THROWN_DISTANCE && 
            abs(new_distances[i] - old_distances[i]) > NOISE_DISTANCE
        ){
            still = false;
            break;
        }
    }
    return still;
}

uint8_t calc_bags_on_board(int32_t board_weight){
    for(uint8_t i=0;i<=NUM_BAGS;i++){
        if(abs(board_weight - BAG_WEIGHT*i) < (BAG_WEIGHT / 2 + 1)){
            return i;
        }
    }
    return 255; // 255 indicates error
}

void game_loop(
    const uint8_t *ids, 
    const int32_t *new_distances, 
    bag_state_t *bag_states,
    int32_t board_weight,
    bool line_break_is_tripped)
{
    /* 
     * -- Explanation of Distances -- 
     * We have 3 sets of distances floating around in this function:
     * new_distances, last_distances, and old_distances. 
     * - The new_distances are the ones we just received. 
     * - The last_distances are the ones we received in the last call to this 
     *   function. 
     * - The old_distances is the last set of distances which we used to update
     *   bag states. 
     * We only update bag states if 2 things are true:
     *  1. The thrown bags aren't currently moving.
     *  2. The thrown bags have changed position since we last updated bag 
     *     states.
     */
    static bool line_break_was_tripped = false;
    static int32_t last_distances[8];
    static int32_t old_distances[8];
    static uint8_t num_bags_through_hole = 0;

    /* make all the bags start as far away as possible */
    static bool first_call = true;
    if(first_call){
        first_call = false;
        for(uint8_t i=0;i<NUM_BAGS;i++){
            last_distances[i] = INT32_MAX;
            old_distances[i] = INT32_MAX;
        }
    }

    if(line_break_is_tripped) line_break_was_tripped = true;

    if(! all_is_still(new_distances, last_distances)){
        /* clean up and return */
        for(uint8_t i=0;i<NUM_BAGS;i++){
            last_distances[i] = new_distances[i];
        }
        return;
    }
    if(all_is_still(new_distances, old_distances)){ 
        /* clean up and return */
        for(uint8_t i=0;i<NUM_BAGS;i++){
            last_distances[i] = new_distances[i];
        }
        return;
    }
    
    /* count bags */
    uint8_t num_bags_on_board = calc_bags_on_board(board_weight);
    if(line_break_was_tripped) num_bags_through_hole++;
    uint8_t num_bags_thrown = 0;
    for(uint8_t i=0;i<NUM_BAGS;i++){
        if(new_distances[i] < THROWN_DISTANCE) num_bags_thrown++;
    }

    /* order bag indices by distance from sensor */
    uint8_t ndx_ordered_by_distance_asc[NUM_BAGS];

    int32_t distance_copy[NUM_BAGS];
    for(uint8_t i=0;i<NUM_BAGS;i++){
        distance_copy[i] = new_distances[i];
    }

    for(uint8_t j=0;j<NUM_BAGS;j++){
        int32_t min_distance = INT32_MAX;
        uint8_t min_ndx = 0;
        for(uint8_t i=0;i<NUM_BAGS;i++){
            if(distance_copy[i] < min_distance){
                min_distance = distance_copy[i];
                min_ndx = i;
            }
        }
        ndx_ordered_by_distance_asc[j] = min_ndx;
        distance_copy[min_ndx] = INT32_MAX;
    }

    /* update the bag states*/
    for(uint8_t i=0;i<num_bags_through_hole;i++){
        uint8_t state_ndx = ndx_ordered_by_distance_asc[i];
        bag_states[state_ndx] = IN_HOLE;
    }

    for(uint8_t i=num_bags_through_hole;i<num_bags_through_hole+num_bags_on_board;i++){
        uint8_t state_ndx = ndx_ordered_by_distance_asc[i];
        bag_states[state_ndx] = ON_BOARD;
    }
    
    for(uint8_t i=num_bags_through_hole+num_bags_on_board;i<num_bags_thrown;i++){
        uint8_t state_ndx = ndx_ordered_by_distance_asc[i];
        bag_states[state_ndx] = OFF_BOARD;
    }
    for(uint8_t i=num_bags_thrown;i<NUM_BAGS;i++){
        uint8_t state_ndx = ndx_ordered_by_distance_asc[i];
        bag_states[state_ndx] = UNTHROWN;
    }

    /* clean up and return */
    line_break_was_tripped = false;
    for(uint8_t i=0;i<NUM_BAGS;i++){
        last_distances[i] = new_distances[i];
        old_distances[i] = new_distances[i];
    }


}

void print_state(const uint8_t *ids, const bag_state_t *bag_states){
    for(int i=0;i<NUM_BAGS;i++){
        printf("%d: ", ids[i]);
        switch(bag_states[i]){
            case UNTHROWN:
                printf("UNTHROWN\n");
                break;
            case ON_BOARD:
                printf("ON BOARD\n");
                break;
            case OFF_BOARD:
                printf("OFF BOARD\n");
                break;
            case IN_HOLE:
                printf("IN HOLE\n");
                break;
            default:
                printf("ERROR: Invalid state.\n");
        }
    }
    printf("\n");
}

int main(){
    /* vars for managing the test */
    FILE *fp = fopen("bag_locations.txt", "r");
    int x = 0;
    int retval = 1;
    int args[NUM_BAGS+2]; // holds distances, weight and line_break_is_tripped
    int arg_ndx = 0;

    /* vars to pass to the game loop */
    int32_t distances[NUM_BAGS];
    uint8_t ids[NUM_BAGS];
    bag_state_t bag_states[NUM_BAGS];
    for(int i=0;i<NUM_BAGS;i++){
        ids[i] = i+1;
        bag_states[i] = UNTHROWN;
    }

    while (retval == 1){
        retval = fscanf(fp, "%i", &x); // get a new int from the file
        args[arg_ndx++] = x;
        if(arg_ndx == NUM_BAGS+2 && retval == 1){
            /* arg buff full; run game loop*/
            int i=0;
            for(;i<NUM_BAGS;i++){
                distances[i] = (int32_t)(args[i]);
            }
            int32_t weight = (int32_t)args[i++];
            bool line_break = (bool)args[i];
            game_loop(ids, distances, bag_states, weight, line_break);
            print_state(ids, bag_states);
            arg_ndx = 0;
        }
    }

    fclose(fp);
    return 0;
}